// dependencies
const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const sassMiddleware = require('node-sass-middleware');

// create routes
const index = require('./routes/index');
const lists = require('./routes/lists');
const about = require('./routes/about');

const app = express();

// view engine EJS
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// favicon public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// sass loader SCSS
app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: false, // true: SASS false: SCSS
  sourceMap: true
}));

app.use(express.static(path.join(__dirname, 'public')));
// set routes
app.use('/', index);
app.use('/about', about);
app.use('/lists', lists);

// 404 handler
app.use(function(req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  //  dev errors
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render errors
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
