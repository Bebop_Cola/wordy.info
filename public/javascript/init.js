// aesthetics
jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></div><div class="quantity-button quantity-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></div></div>').insertAfter('.quantity input');
jQuery('.quantity').each(function () {
    let spinner = jQuery(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

    btnUp.click(function () {
        let oldValue = parseFloat(input.val()), newVal;
        if (oldValue >= max) {
            newVal = oldValue;
        } else {
            newVal = oldValue + 1;
            $(".quantity input").css('background-color', '#1abc9c');
            btnUp.css('color', '#1abc9c');
            btnDown.css('color', '#1abc9c');
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
    });

    btnDown.click(function () {
        let oldValue = parseFloat(input.val()), newVal;
        if (oldValue <= min) {
            newVal = oldValue;
        } else {
            newVal = oldValue - 1;
            if (newVal == min) {
                $(".quantity input").css('background-color', '#CCCCCC');
                btnDown.css('color', '#CCCCCC');
                btnUp.css('color', '#CCCCCC');
            }
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
    });

});
document.querySelector('#year-check').addEventListener('change', function (e) {
    if ($('#year-check').is(':checked')) {
        $('.sub-group').css('opacity', '1');
        $('.sub-group').css('height', 'auto');
        $('.sub-group').css('overflow', 'visible');
    } else {
        $('.sub-group').css('opacity', '0');
        $('.sub-group').css('height', '0');
        $('.sub-group').css('overflow', 'hidden');
    }
});
document.querySelector('#leet-check').addEventListener('change', function (e) {
    if ($('#leet-check').is(':checked')) {
        $('#leet-edit').css('opacity', '1');
    } else { 
        $('#leet-edit').css('opacity', '0');
    }
});
const elements = $('.modal-overlay, .modal');

$('#leet-edit').click(function(){
    elements.addClass('active');
});

$('.close-modal').click(function(){
    elements.removeClass('active');
});

window.addEventListener('load', 
  function() { 
    $('#loaddiv').css('display', 'none');
  }, false);
