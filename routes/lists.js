const express = require('express');
const router = express.Router();

/* lists page. */
router.get('/', function(req, res, next) {
  res.render('lists', { title: 'Lists' });
});

module.exports = router;
